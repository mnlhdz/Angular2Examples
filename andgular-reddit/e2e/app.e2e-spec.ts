import { AndgularRedditPage } from './app.po';

describe('andgular-reddit App', () => {
  let page: AndgularRedditPage;

  beforeEach(() => {
    page = new AndgularRedditPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
